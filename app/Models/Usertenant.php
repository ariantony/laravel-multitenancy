<?php

namespace App\Models;

use App\Models\Tenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Usertenant extends Model
{
    use HasFactory;

    public static function booted() {
        static::created(function($usertenant){
            $userTenants = Tenant::create([
                'id' => $usertenant->domain
            ]);
            $userTenants->domains()->create([
                'domain' => $usertenant->domain . '.' . env('APP_CENTRAL_DOMAIN')
            ]);
        });
    }
}
